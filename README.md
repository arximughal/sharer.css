Sharer.css | CSS Extension for Sharer.js
-----------------------------------------------------------------------------
#### Beautiful Sharer.js buttons with CSS3 only : [DEMO]

A small extention for the plugin [Sharer.js] by [Ellison Leão].

------------------------------------------------------------------------

What’s included?
----------------

The package consists of a small `sharer.css`  file. Use this file alongside with [Sharer.js] to get 25+ beautiful share buttons to insert on your webpages. 

How to?
-------

When using [Sharer.js], just use `sharer-btn` class with every sharer.js `<button>`.

  [Sharer.js]: https://github.com/ellisonleao/sharer.js
  [Ellison Leão]: https://github.com/ellisonleao
  [DEMO]: https://arximughal.github.io/sharer.css/
